json.extract! training, :id, :training_type, :created_at, :updated_at
json.url training_url(training, format: :json)
