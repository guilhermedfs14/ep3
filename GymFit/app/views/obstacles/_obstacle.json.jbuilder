json.extract! obstacle, :id, :obstacle, :created_at, :updated_at
json.url obstacle_url(obstacle, format: :json)
