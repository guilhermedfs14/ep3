json.extract! exercise, :id, :appliance, :sessions, :repetition, :created_at, :updated_at
json.url exercise_url(exercise, format: :json)
