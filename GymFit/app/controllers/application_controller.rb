class ApplicationController < ActionController::Base
   before_action :configure_permitted_parameters, if: :devise_controller?
 
   protected
   def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :weight, :height, :mass_percentage, :fat_percentage, :email, :password, :remember_me])
      devise_parameter_sanitizer.permit(:sign_in, keys: [:email, :password, :remember_me])
      devise_parameter_sanitizer.permit(:account_update, keys: [:name, :weight, :height, :mass_percentage, :fat_percentage, :email, :password, :remember_me])
   end
end
