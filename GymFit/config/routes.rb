Rails.application.routes.draw do
  get 'admins/show'
  get 'users/show'
  devise_for :admins
  devise_for :users
  resources :users
  resources :admins
  resources :trainings
  resources :exercises
  resources :obstacles
  resources :goals
  get 'homepage/index'
  get 'info' => 'information#index'
  get 'listaalunos' => 'users#list_users' 

  root 'homepage#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
