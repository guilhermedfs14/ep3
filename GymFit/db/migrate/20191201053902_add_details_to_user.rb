class AddDetailsToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :name, :string
    add_column :users, :weight, :float
    add_column :users, :height, :float
    add_column :users, :mass_percentage, :float
    add_column :users, :fat_percentage, :string
  end
end
