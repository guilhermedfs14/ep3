class CreateObstacles < ActiveRecord::Migration[6.0]
  def change
    create_table :obstacles do |t|
      t.string :obstacle

      t.timestamps
    end
  end
end
