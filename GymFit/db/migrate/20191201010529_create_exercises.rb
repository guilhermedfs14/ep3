class CreateExercises < ActiveRecord::Migration[6.0]
  def change
    create_table :exercises do |t|
      t.string :appliance
      t.integer :sessions
      t.integer :repetition

      t.timestamps
    end
  end
end
