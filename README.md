# Descrição da Solução

* Objetivos
    * O objetivo principal foi criar uma pagna web para auxiliar uma academia, onde o professor seria capaz de atualizar as informações do aluno e o aluno pudesse ver essas atualizações
* Funcionalidades
    1. Visualizar Informações do aluno;
    2. Adicionar um novo treino;
    3. Adicionar um novo exercicio;
    4. Editar informações sobre a massa e a gordura corporal do aluno.

# Requisitos
1. Há 6 models: user, admin, exercise, goal, obstacle e  training. A model training pode ter vários exercises
2. Há diferentes níveis de permissões para diferentes tipos de usuários. O usuário público(não cadastrado) pode visualizar todos os nomes dos treinos. O usuário cadastrado regular pode  visualizar suas informações como peso  massa gordura. O usuário administrador pode criar novos treinos e exercicios, pode visualizar as informações de todos os alunos e editar suas informações de massa e gordura corporal.
3. Utilizou-se gems para além do padrão rails, tais como: devise e bootstrap.
4. Na elaboração da inteface utilizou-se bootstrap, html e CSS.

# Como executar
1. Instale a versao 2.6.3 do ruby 
2. Instale a versao 6.0.0 do Ruby on Rails com o comando ``` gem install rails --version 6.0.0 ```
3. Dentro da pasta do projeto entre na pasta GymFit
4. Execute o comando ``` bundle install ``` para instalar todas as gens do projeto
5. Execute o comando ```rails db:migrate``` para atualizar o banco de dados
6. Execute o comando ```rails s``` para rodar o projeto 
7. No navegador, vá para "//localhost:3000"
8. Caso o comando ```rails s ```apresente um erro, execute o comando ```yarn install --check-files ```.

# Versões utilizadas na elaboração do projeto
* ruby 2.6.3
* Rails 6.0.0
* Bundler version  1.17.2
